import java.util.Scanner;

class Section3_7 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter a Number : ");
		int a = sc.nextInt();

		if(a < 0) System.out.print("-1");
		else if (a > 0) System.out.print("1");
		else System.out.print("0");
	}
}