import java.util.Scanner;

class Section3_20 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		int arr[] = new int[5];

		System.out.println("Enter 5 Numbers: ");

		int max = arr[0];
		int prev = 0;

		for(int i=0;i<5;i++)
			arr[i] = sc.nextInt();

		for(int i=1;i<5;i++)
		{
			if(arr[i] > max)
				max = arr[i];
		}

		prev = arr[0];

		for(int i=1;i<5;i++)
		{
			if(arr[i] > prev && arr[i] < max)
				prev = arr[i];
		}

		System.out.println("Second largest No. :" + prev);

	}
}