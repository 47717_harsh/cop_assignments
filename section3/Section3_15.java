import java.util.Scanner;

class Section3_15 {
	
	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter a Number : ");		
		int num = sc.nextInt();

		if(num%3 == 0 && num%5 == 0)
			System.out.print("Number is divisible by 3 and 5");
		else System.out.print("Not divisible by 3 and 5");
	}
}