import java.util.Scanner;

class Section3_10 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Five Numbers : ");
		int a = sc.nextInt();
		int b = sc.nextInt();
		int c = sc.nextInt();
		int d = sc.nextInt();
		int e = sc.nextInt();

		if(a < b && a < c && a<d && a<e) System.out.print(a + " is smallest");
		else if (b < a && b < c && b<d && b<e) System.out.print(b+" is smallest");
		else if (c < a && c < b && c<d && c<e) System.out.print(c+" is smallest");
		else if (d < a && d < c && d<b && d<e) System.out.print(d+" is smallest");
		else System.out.print(e+" is smallest");
		
	}
}