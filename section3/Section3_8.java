import java.util.Scanner;

class Section3_8 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Two Numbers : ");
		int a = sc.nextInt();
		int b = sc.nextInt();

		if(a > b) System.out.print(a + " is greater");
		else if (b > a) System.out.print(b+" is greater");
		else System.out.print("both are equal");
	}
}