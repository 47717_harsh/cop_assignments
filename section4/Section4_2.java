import java.util.Scanner;

class Section4_2 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		int arr[] = new int[10];

		System.out.println("Enter 10 Numbers : ");
		for(int i=0;i<10;i++)
			arr[i] = sc.nextInt();

		int sum = 0;

		for(int i = 0;i<10;i++)
		{
			sum = sum + arr[i];
		}

		System.out.println("\nSum : " + sum);
		System.out.println("Average : " + sum/10 );
	}
}