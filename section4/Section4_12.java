import java.util.Scanner;

class Section4_12 {


	static int cube(int num)
	{
		return num*num*num;
	}

	static int sumOfCube(int num)
	{
		int sum = 0;

		while(num != 0)
		{
			sum = sum + cube(num%10);
			num = num/10;
		}
		return sum;
	}
	public static void main(String[] args) {
		
		System.out.print("Enter a Number : ");

		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();

		int sum = sumOfCube(num);


		if(num == sum)
			System.out.println("Armstrong Number");
		else 
			System.out.println("Not a Armstrong Number");
	}
}