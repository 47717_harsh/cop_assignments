import java.util.Scanner;

class Section4_15 {
	

	public static void main(String[] args) {
		
		System.out.print("Enter a Number : ");

		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		int rev = 0;
		while(num != 0)
		{
			rev = (10*rev) + (num%10);
			num = num/10;
		}

		System.out.print("Reverse : "+rev);

	}
}