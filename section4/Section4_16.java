import java.util.Scanner;

class Section4_16 {
	
	static int reverseNo(int num)
	{
		int rev = 0;
		while(num != 0)
		{
			rev = (10*rev) + (num%10);
			num = num/10;
		}
		return rev;
	}
	public static void main(String[] args) {
		
		System.out.print("Enter a Number : ");

		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		
		if(num == reverseNo(num))
			System.out.print("Palindrome");
		else
			System.out.print("Not a Palindrome");

	}
} 