import java.util.Scanner;

class Section4_18 {
	
	
	public static void main(String[] args) {
		
		System.out.print("Enter a String : ");

		Scanner sc = new Scanner(System.in);
		String st = sc.nextLine();
		char str[] = st.toCharArray();
		int count=0;

		for(char c:str)
		{
			count++;
		}
		System.out.print(count);
	}
}