class Emp {

	private int empId;
	private String name;
	private String department;
	private int salary;
	private String address;

	Emp()
	{
		empId = 1234;
		name = "harsh";
		department = "development";
		salary = 55000;
		address = "bhanpura";
	}

	void displayEmpDetails()
	{
		System.out.println("Employee ID : "+empId);
		System.out.println("Employee name : "+name);
		System.out.println("Employee department : "+department);
		System.out.println("Employee salary : "+salary);
		System.out.println("Employee address : "+address);
	}
}

public class Section5_1 {

	public static void main(String[] args) {
		
		Emp e1 = new Emp();

		e1.displayEmpDetails();
	}
}