import java.util.Scanner;

class Circle {

	private int radius;

	Circle(int radius)
	{
		this.radius = radius;
	}

	double calculateArea()
	{
		return 3.14*radius*radius;
	}
	double calculatePerimiter()
	{
		return 2*3.14*radius;
	}
}

class Section5_2 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Radius : ");
		int radius = sc.nextInt();

		Circle c1 = new Circle(radius);

		System.out.println("Area : " + c1.calculateArea());
		System.out.println("Perimiter : " + c1.calculatePerimiter());
	}
}