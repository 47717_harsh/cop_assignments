import java.util.Scanner;

class section2_5 {
	
	public static void main(String[] args) {
	
		System.out.print("Enter length and width of rectangle : ");

		Scanner sc = new Scanner(System.in);

		int length = sc.nextInt();
		int width = sc.nextInt();

		System.out.println("Area      : " + (length*width));
		System.out.println("Perimeter : " + (2*(length+width)));		

	}
}