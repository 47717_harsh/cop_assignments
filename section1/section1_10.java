class section1_10 {
	
	public static void main(String[] args) {
		
		double n1 = Double.parseDouble(args[0]);
		double n2 = Double.parseDouble(args[1]);

		System.out.println("Addition       : "+ (n1+n2));
		System.out.println("Substraction   : "+ (n1-n2));
		System.out.println("Multiplication : "+ (n1*n2));
		System.out.println("Division       : "+ (n1/n2));
	}
}